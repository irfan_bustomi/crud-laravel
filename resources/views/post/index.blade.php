@extends('adminLTE.master')

@section('content')
    <div class="mt-2 ml-3">
        <div class="card-header">
            <h3 class="card-title">Post Table</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success')}}
                </div>
            @endif
            <a href="/post/create" class="btn btn-primary mb-2">Tambah</a>
            <table class="table table-bordered">
              <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Body</th>
                    <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
                @forelse($post as $key => $value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->title}}</td>
                    <td>{{$value->body}}</td>
                    <td style="display:flex">
                        <a href="/post/{{$value->id}}" class="btn btn-info btn-sm">Show</a>&nbsp;
                        <a href="/post/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>&nbsp;
                        <form action="/post/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                    <tr colspan="3">
                        <td colspan="4" align="center">No data</td>
                    </tr>  
                @endforelse
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
    </div>
@endsection