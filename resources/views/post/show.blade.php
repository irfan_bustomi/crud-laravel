@extends('adminLTE.master')

@section('content')
    <div class="mt-3 ml-3">
        <h2>Show Post {{$post->id}}</h2>
        <h4>{{$post->title}}</h4>
        <p>{{$post->body}}</p>
    </div>
@endsection